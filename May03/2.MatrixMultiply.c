//
// Created by naman on 4/5/22.
//

#include <stdio.h>
#include <stdlib.h>

int main() {
    int h1 = 0, h2 = 0, w1 = 0, w2 = 0;
    printf("Enter Row and Columns of first matrix : ");
    scanf(" %d %d", &h1, &w1);
    printf("Enter Row and Columns of second matrix : ");
    scanf(" %d %d", &h2, &w2);

    // Check If matrix multiplication is possible
    if (w1 != h2){
        printf("Sorry, matrix multiplication not possible !");
        return 0;
    }

    int **matrix1 = (int **) malloc(h1*sizeof(int *));
    int **matrix2 = (int **) malloc(h2*sizeof(int *));

    printf("Enter Matrix 1 \n");
    for (int row = 0; row < h1; row++){
        matrix1[row] = (int *) malloc(w1 * sizeof(int));
        for (int column = 0; column < w1; column++)
            scanf(" %d", &matrix1[row][column]);
    }
    printf("Enter Matrix 2 \n");
    for (int row = 0; row < h2; row++){
        matrix2[row] = (int *) malloc(w2 * sizeof(int));
        for (int column = 0; column < w2; column++)
            scanf(" %d", &matrix2[row][column]);
    }

    // Multiply Matrix

    int ** matrix3 = (int **) malloc(h1 * sizeof(int *));

    for (int row = 0; row < h1; row++){
        matrix3[row] = (int *) malloc(w2 * sizeof(int));
        for (int column = 0; column < w2; column++){
            matrix3[row][column] = 0;
            for(int i = 0; i < w1; i++){
                matrix3[row][column] += matrix1[row][i] * matrix2[i][column];
            }
        }
    }

    for (int row = 0; row < h1; row++){
        for (int column = 0; column < w2; column++){
            printf(" %d", matrix3[row][column]);
        }
        printf("\n");
    }
}