//
// Created by naman on 5/5/22.
//
// IV. Implement the above program for Employee structures (contains employee_name,
//emp_no, emp_salary).

#include <stdio.h>
#include <stdlib.h>

// Declaring Employee  structure

struct Employee{
    char employee_name[100];
    long long int emp_no;
    long long int salary;
} *empPtr;

void Display(struct Employee * ptr, int length){
    for (int i = 0; i < length; i++){
        printf("\n==============================================================");
        printf("\nEmployee name : %s", (ptr+i)->employee_name);
        printf("\nEmployee number : %lld", (ptr+i)->emp_no);
        printf("\nEmployee salary : %lld", (ptr+i)->salary);
    }
    printf("\n==============================================================");
}

int number = 0;
void takeInput() {
    // Input   employees from the user

    printf("Enter number of employees : ");
    scanf(" %d", &number);
    empPtr = (struct Employee *) malloc(number * sizeof (struct Employee));
    for (int i = 0; i<number; i++){
        printf("Enter employee name : ");
        scanf(" %s", empPtr[i].employee_name);
        printf("Enter employee number: ");
        scanf(" %lld", &empPtr[i].emp_no);
        printf("Enter employee salary : ");
        scanf(" %lld", &empPtr[i].salary);
    }
}

int main() {
    takeInput();
    int choice = 0;
    while (choice != 3){
        // Enter choice
        printf("\n1 -> Enter employees\n 2 -> Display current database\n 3 -> Quit\n Enter choice :  ");
        scanf(" %d", &choice);

        if (choice == 1)
            takeInput();
        else if (choice == 2)
            Display(empPtr, number);
        else if (choice == 3){
            printf("\nThank You");
            break;
        } else
            printf("\nSorry, wrong option. Please Try again !");


    }
}