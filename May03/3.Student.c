//
// Created by naman on 4/5/22.
//
// III. WAP to store an information in array of structure, dynamically, and also give a function
//to display the current information. The program should give user a choice for inserting a
//data or to display the current data. Implement the program for Student structures
//(contains student_name, student_roll_no, total_marks).


#include <stdio.h>
#include <stdlib.h>

// Declaring Student structure

struct Student{
    char student_name[100];
    long long int student_roll_no;
    int total_marks;
} *stuPtr;

void Display(struct Student * ptr, int length){
    for (int i = 0; i < length; i++){
        printf("\n==============================================================");
        printf("\nStudent name : %s", (ptr+i)->student_name);
        printf("\nStudent roll number : %lld", (ptr+i)->student_roll_no);
        printf("\nStudent total marks : %d", (ptr+i)->total_marks);
    }
    printf("\n==============================================================");
}

int number = 0;
void takeInput() {
    // Input   students from the user

    printf("Enter number of students : ");
    scanf(" %d", &number);
    stuPtr = (struct Student *) malloc(number * sizeof (struct Student));
    for (int i = 0; i<number; i++){
        printf("Enter student name : ");
        scanf(" %s", stuPtr[i].student_name);
        printf("Enter student roll number : ");
        scanf(" %lld", &stuPtr[i].student_roll_no);
        printf("Enter student total marks : ");
        scanf(" %d", &stuPtr[i].total_marks);
    }
}

int main() {
    takeInput();
    int choice = 0;
    while (choice != 3){
        // Enter choice
        printf("\n1 -> Enter students\n 2 -> Display current database\n 3 -> Quit\n Enter choice :  ");
        scanf(" %d", &choice);

        if (choice == 1)
            takeInput();
        else if (choice == 2)
            Display(stuPtr, number);
        else if (choice == 3){
            printf("\nThank You");
            break;
        } else
            printf("\nSorry, wrong option. Please Try again !");


    }
}