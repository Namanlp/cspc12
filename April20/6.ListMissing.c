//
// Created by naman on 20/04/22.
//
// VI. WAP to identify the missing numbers in a given Array within the range [1...N].

#include <stdio.h>

int main(){
    // Input array length and array
    int length=0;
    printf("Enter length of array  : ");
    scanf("%d", &length);
    long long int array[length];

    printf("Enter Array : ");
    for(int i =0; i < length; i++)
        scanf("%lld", &array[i]);
    printf("Missing numbers are : ");

    for (int i =1; i<= length; i++){
        int found = 0;
        for(int j =0; j<length; j++)
            // If number is found in array, break the loop
            if (array[j] == i ){
                found = 1;
                break;
            }
        if (! found)
            printf(" %d ", i); // Print if number is missing
    }
}