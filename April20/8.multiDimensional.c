//
// Created by naman on 27/4/22.
//
// 1. WAP to take input of character matrix and create a function ‘givestr’ to get string column wise
//and row-wise and print all string in sorted manner

#include <stdio.h>

void swap(char* xp, char* yp)
{
    char temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void selectionSort(char arr[], int n)
{
    int i, j, min_idx;
    for (i = 0; i < n - 1; i++) {
        min_idx = i;
        for (j = i + 1; j < n; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;
        swap(&arr[min_idx], &arr[i]);
    }
}

int rows = 0, columns = 0;
void givestr( const char givenMatrix[rows][columns] ){
    printf("Row-wise\n");
    for (int row=0; row<rows; row++){
        char rowString[columns];
        for (int column = 0; column < columns; column++){
            rowString[column] = givenMatrix[row][column];
        }
        selectionSort(rowString, columns);

        for (int column = 0; column < columns; column++){
            printf(" %c", rowString[column]);
        }
        printf("\n");
    }

    printf("\nColumn-wise\n");
    for (int column=0; column<columns; column++){
        char columnString[rows];
        for (int row = 0; row < rows; row++){
            columnString[row] = givenMatrix[row][column];
        }
        selectionSort(columnString, rows);
        for (int row = 0; row < rows; row++){
            printf(" %c", columnString[row]);
        }
        printf("\n");
    }
}

int main(){
    rows = 0, columns = 0;
    printf("Enter number of rows and columns : ");
    scanf(" %d %d", &rows, &columns);
    char matrix[rows][columns];
    for (int row=0; row < rows; row++){
        printf("Enter row number %d : ", row);
        for (int column=0; column<columns; column++)
            scanf(" %c", &matrix[row][column]);
    }
    givestr(matrix);

}