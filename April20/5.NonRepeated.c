//
// Created by naman on 20/04/22.
//
//V. WAP to find the number of Non Repeated elements in an Array.

#include <stdio.h>

int main(){
    // Input array length and array
    int length=0;
    printf("Enter length of array  : ");
    scanf("%d", &length);
    long long int array[length];

    printf("Enter Array : ");
    for(int i =0; i < length; i++) {
        scanf("%lld", &array[i]);
    }

    int count =0;
    for (int i =0; i<length; i++){
        int isUnique = 1;
        for (int j=0; j<length; j++){
            if (array[i] == array[j] && i != j){ // stop if same element found in array
                isUnique = 0;
                break;
            }
        }
        if (isUnique) // Add to counter if element is unique
            count++;
    }

    printf("There are %d non repeated elements in the given array. ", count);
}