//
// Created by naman on 20/04/22.
//
// I. WAP with function to swap two numbers using call by reference.

#include <stdio.h>

// Swap numbers by reference
void swapNumbers(long long int *num1, long long int *num2){
    long long int *temp = num1;
    num1 = num2;
    num2 = temp;
}

int main(){
    long long int number1 = 0, number2 = 0;
    printf("Enter numbers : ");
    scanf("%lld %lld", &number1, &number2);

    printf("\nNumbers Before swapping are : %lld %lld", number1, number2);
    swapNumbers(&number1, &number2);
    printf("\nNumbers After swapping are : %lld %lld", number2, number1);

}