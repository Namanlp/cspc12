//
// Created by naman on 1/6/22.
//

#include <stdio.h>

// --------------------------------------------------- Printing Array ----------------------------------------------------

void printArray(int array[], int length){
    printf(" [ ");
    for (int i = 0; i< length - 1; i++)
        printf("%d , ", array[i]);
    printf("%d ]", array[length-1]);
}

// --------------------------------------------------- Insertion Sort -----------------------------------------------------

void insertionSort(int array[], int length){
    int count = 0;
    int element = 0, j = 0;
    for (int i = 1 ; i < length ; i++){
        element = array[i];
        for ( j = i-1; j >0 && array[j] > element ; j--){
            array[j+1] = array[j];
            count++;
        }
        array[j] = element;
        }

}

int main(){
    int length = 0;
    printf("Enter length of array : ");
    scanf(" %d", &length);
    int array[length];
    printf("Enter array : ");
    for (int i = 0; i < length; i++)
        scanf(" %d", &array[i]);

    printf("Unsorted array : ");
    printArray(array, length);
    insertionSort(array, length);
    printf("\n Sorted array : ");
    printArray(array, length);

}