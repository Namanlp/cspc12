//
// Created by naman on 11/5/22.
//
//II. WAP to implement Bubble on 1D array of Faculty structures (contains
//faculty_name, faculty_ID, subject_codes, class_names) with key as faculty_ID.

#include <stdio.h>

struct Faculty{
    char faculty_name[100];
    int faculty_ID;
    char subject_codes[100];
    char class_names[100];
};

// Function to swap 2 structures in an array
void swapFaculty(struct Faculty * f1, struct Faculty * f2){
    struct Faculty FCopy = *f1;
    *f1 = *f2;
    *f2 = FCopy;
}

void bubbleSort(struct Faculty array[], int length){
    int isArranged = 0;
    for(int i = 1; i<length && ! isArranged;i++){
        isArranged = 1;
        for(int j = 0; j<length-i; j++){
            if (array[j].faculty_ID > array[j+1].faculty_ID){
                swapFaculty(&array[j], &array[j+1]);
                isArranged = 0;
            }
        }
    }
}


int main(){
    int length = 0;
    printf("Enter number of faculties : ");
    scanf(" %d", &length);
    struct Faculty facultyArray[length];

    for (int i =0; i<length; i++){
        printf("\nEnter Faculty name, ID, Subject codes and class names : ");
        scanf(" %s %d %s %s", facultyArray[i].faculty_name, &facultyArray[i].faculty_ID, facultyArray[i].subject_codes, facultyArray[i].class_names);
    }

    bubbleSort(facultyArray, length);

    for (int i =0; i<length; i++){
        printf("\n======================================================");
        printf("\nFaculty name : %s ", facultyArray[i].faculty_name);
        printf("\nFaculty ID : %d ", facultyArray[i].faculty_ID);
        printf("\nFaculty Subject code is : %s ", facultyArray[i].subject_codes);
        printf("\nFaculty class names are : %s ", facultyArray[i].class_names);
    }
}