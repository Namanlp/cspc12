//
// Created by naman on 8/6/22.
//

#include <stdio.h>
#include <stdlib.h>

struct DoubleLL{
    struct  DoubleLL * prev;
    int data;
    struct  DoubleLL * next;
};

// ---------------------------------------------------------- Get element data and key  ----------------------------------------------------------------------

struct DoubleLL * getElement(){
    struct DoubleLL * temp = (struct DoubleLL * ) malloc(sizeof( struct  DoubleLL));
    printf("\nEnter element : ");
    scanf(" %d", &(temp->data));
    return temp;
}

int getKey(){
    int key = 0;
    printf("\nEnter key : ");
    scanf(" %d", &key);
    return key;
}


// ---------------------------------------------------------- Find Size of Double Linked List  ----------------------------------------------------------------------

int getSize(struct DoubleLL * head){
    struct DoubleLL * temp = head->next;
    int counter = 1;
    while(temp != head){
        counter++;
        temp = temp->next;
    }
    return counter;
}

// ---------------------------------------------------------- Accessing a node ----------------------------------------------------------------------

struct DoubleLL * accessingNode(struct DoubleLL * head, int key){

    struct DoubleLL * traverse = head;
    int counter = 0;

    while (traverse->data != key && traverse->next != NULL){
        counter++;
        traverse = traverse->next;
    }

    if (traverse->data != key){
        printf("\nSorry, key not found");
        return NULL;
    }

    printf("\nGiven key is %d ahead of head", counter);
    return traverse;
}


// ---------------------------------------------------------- Adding Node Before key ----------------------------------------------------------------------

void addNodeBefore(struct DoubleLL * head,struct DoubleLL * element, int key){
    struct DoubleLL * traverse = accessingNode(head, key);

    if (traverse == NULL)
        return;

}

// ---------------------------------------------------------- Adding Node After key ----------------------------------------------------------------------

void addNodeAfter(struct DoubleLL * head,struct DoubleLL * element, int key){
    struct DoubleLL * traverse = accessingNode(head, key);

    if (traverse != NULL){
        struct DoubleLL * temp = traverse->next->next;
        traverse->next->next = element;
        element->next = temp;
    }
}

// ----------------------------------------------------------  Deleting complete list ----------------------------------------------------------------------

void deleteList(struct DoubleLL * head){
    struct DoubleLL * traverse = head;
    struct DoubleLL * temp = head->next;
    while (temp != NULL){
        free(traverse);
        traverse = temp;
        temp = temp->next;
    }
    head->next = NULL;
}

// ---------------------------------------------------------- Displaying the list ----------------------------------------------------------------------

void displayList(struct DoubleLL * head){
    if (head->next == NULL){
        printf("List is empty !");
        return;
    }
    struct DoubleLL * traverse = head->next;

    printf("\n[ %d", head->data);
    do {
        printf(", %d", traverse->data);
        traverse = traverse->next;
    }while (traverse != NULL);

    printf(" ]");
}

// ----------------------------------------------------------  Magic Starts here ----------------------------------------------------------------------

int main(){
    struct DoubleLL * head = (struct DoubleLL * ) malloc(sizeof( struct  DoubleLL));
    head->prev = NULL;
    head->data = 10;
    head->next = (struct DoubleLL * ) malloc(sizeof( struct  DoubleLL));
    head->next->prev = head;
    head->next->data = 20;
    head->next->next = NULL;

    displayList(head);

    struct DoubleLL * newElement= (struct DoubleLL * ) malloc(sizeof( struct  DoubleLL));
    newElement->data = 0;

    int choice;
    do{
        printf( "\n==============================================================================" );
        printf( "\n1 : Insert an element into linked list before key." );
        printf( "\n2 : Insert an element into linked list after key." );
        printf( "\n3 : Accessing a node (finding the position wrt header)." );
        printf( "\n4 : Accessing a node (finding the position wrt header)." );
        printf( "\n5 : Remove an element from linked list with key." );
        printf( "\n6 : Complete Deletion of list." );
        printf( "\n7 : Displaying the current list in clockwise fashion." );
        printf( "\n8 : Displaying the current list in anticlockwise fashion." );
        printf( "\n9 : Sorting the list." );
        printf( "\n10 : Quit." );

        printf( "\nEnter Choice : ");
        scanf(" %d", &choice);


        printf( "\n==============================================================================" );

        switch (choice) {
            case 1 : addNodeBefore(head, getElement(), getKey()); break;
            case 2 : addNodeAfter(head, getElement(), getKey()); break;
            case 3: accessingNode(head, getKey()); break;
//            case 4: removeNode(head, getKey()); break;
            case 5 : deleteList(head); choice = 10 ;break;
            case 6: displayList(head); break;
//            case 7: swapNodes(&head, &(head->next)); break;
            case 10: printf("\nThank You"); deleteList(head) ; break;
            default: printf( "\nWrong Choice ! Please enter a valid choice;" );
        }
    } while (choice != 10);
}