//
// Created by naman on 1/6/22.
//

#include <stdio.h>
#include <stdlib.h>

struct node{
    int number;
    struct node * next;
};

//--------------------------------------------- With Auxiliary Pointer ----------------------------------

struct node * reverseWithAux(struct node * head){

    struct node * cur = head;
    struct node * prev = NULL;

    while (cur != NULL){
        struct node * temp = cur->next;
        cur->next = prev;
        prev = cur;
        cur = temp;
    }
    return prev;
}


//------------------------------------------------- Magic starts here ------------------------------------------------

int main(){
    int length = 0;
    printf("Enter length of list : ");
    scanf(" %d", &length);
    printf("Enter array : ");

    struct node * n1 = malloc(sizeof(struct node));
    struct node * head = n1;

    // ---------------------------------- Input and make Linked List ---------------------------------------------

    for (int i = 0; i< length; i++){
        scanf(" %d", &head->number);
        head->next = malloc(sizeof(struct node));
        head = head->next;
    }
    head->next = NULL;

    // ---------------------------------- Print Linked List ----------------------------------------------------------

    printf("\nBefore reversing : ");
    head = n1;
    for (int i = 0; i< length; i++){
        printf(" %d", head->number);
        head = head->next;
    }

    n1 = reverseWithAux(n1);

    printf("\nAfter reversing : ");
    head = n1;
    for (int i = 0; i< length; i++){
        printf(" %d", head->number);
        head = head->next;
    }

}