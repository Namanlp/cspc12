//
// Created by naman on 25/5/22.
//

#include <stdio.h>
#include <stdlib.h>

struct node{
    int age;
    struct node * next;
};

 void accessNode(struct node * head, int length, int key){
    struct node * currentNode = head;
    for(int i = 0; i< length ; i++){
        if (currentNode->age == key){
            printf("\nElement found at position : %d", i+1);
            break;
        }
        else
            currentNode = currentNode->next;
    }
}

void removeNode(struct node * head, int *length, int key){

    struct node * currentNode = head;
    for(int i = 0; i< *length ; i++){
        if (currentNode->next->age == key){
            currentNode->next = currentNode->next->next;
            *length-=1;
        }
        else
            currentNode = currentNode->next;
    }
}

void deleteList(struct node * head, int length){
    struct node * currentNode = head;
    struct node * nextNode;
    for(int i = 0; i< length ; i++){
        nextNode = currentNode->next;
        free(currentNode);
        currentNode = nextNode;
    }
}

void displayList(struct node * head, int length){
    struct node * currentNode = head;
    printf("\nCurrent List is : ");
    for(int i = 0; i<length; i++){
        printf(" %d ", currentNode->age);
        currentNode = currentNode->next;
    }
}

long node_Size = sizeof(struct node);

int main(){
    printf("Enter number of elements : ");
    int num = 0;
    scanf(" %d", &num);

    printf("\nEnter Array : ");
    struct node * head = (struct node *)malloc(node_Size);
    struct node * current_ptr = head;
    for(int i = 0; i<num;i++){
        scanf(" %d", &current_ptr->age);
        current_ptr->next = (struct node *) malloc(node_Size);
        current_ptr = current_ptr->next;
    }

    int key = 0;
    char choice = ' ';
    while (choice != 'q'){
        printf("\nb-> Accessing a node (finding the position wrt header),\n"
               "c-> Removing a node with particular key value,\n"
               "d-> Complete deletion of a linked list, and\n"
               "e-> Displaying the current list.\n"
               "f-> Copy the linked list and return the pointer of the new list.");
        printf("\nEnter Choice : ");
        scanf(" %c", &choice);
        switch (choice) {
            case 'b' : printf("Enter key : "); scanf(" %d", &key); accessNode(head, num, key); break;
            case 'c' : printf("Enter key : "); scanf(" %d", &key); removeNode(head, &num, key); break;
            case 'd': deleteList(head, num) ; printf("Thank You"); choice = 'q' ;  break;
            case 'e': displayList(head, num); break;
            default: printf("\nWrong Choice. Try Again ! \n"); break;
        }
    }


}