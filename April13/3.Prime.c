//
// Created by naman on 13/04/22.
//
// III. Implement a C Program to Check Whether a Number is Prime or Not.

#include <stdio.h>

int main(){
    int number=0;
    printf("Enter the number to check prime : ");
    scanf(" %d", &number);
    int n = 2, isPrime = 1;
    // stop if factor found or number is greater than square
    while (isPrime && n*n <= number){
        if(number % n == 0)
            isPrime = 0;
        n++;
    }
    if (isPrime)
        printf("Yes, number is prime number");
    else
        printf("No, number is not a prime number");
}