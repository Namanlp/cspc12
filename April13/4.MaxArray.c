//
// Created by naman on 13/04/22.
//
// IV. Implement a C Program to Find the Largest Element in an Array.

#include <stdio.h>

int main(){
    int lenArray = 0;
    printf("Enter length of Array : ");
    scanf(" %d", &lenArray);
    printf("Enter array : ");
    int givenArray[lenArray];
    for (int i = 0; i < lenArray; i++)
        scanf(" %d", &givenArray[i]);
    int max = givenArray[0];
    for (int i =1; i<lenArray;i++)
        if(givenArray[i] > max)
            max = givenArray[i];
    printf("Maximum number in array is : %d", max);
}