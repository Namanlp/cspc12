//
// Created by naman on 13/04/22.
//
//II. Implement a C Program to Check Whether a Number is Palindrome or Not

#include <stdio.h>

int main(){
    int number=0, number_reverse=0;
    printf("Enter the number to check Palindrome : ");
    scanf(" %d", &number);
    int temp = number;
    while (number!=0){
        number_reverse = number_reverse*10 + number%10  ;
        number /= 10;
    }

    if (temp == number_reverse)
        printf("Yes, number is a palindrome. ");
    else
        printf("No, number is not a palindrome. ");

}