//
// Created by naman on 13/04/22.
//
// V. Implement a C program to print alphabet triangle.

#include <stdio.h>

int main(){
    int lines = 0;
    printf("Enter number of lines : ");
    scanf(" %d", &lines);

    if (lines <= 26 ){
        for (int i = 0; i < lines; i++){
            for (int j = 0; j < lines - i; j++ )
                printf(" ");
            for (int j = 0; j <= i; j++)
                printf("%c", 65+j);
            for (int j = i-1; j >= 0; j--)
                printf("%c", 65+j);

            printf("\n");
        }
    }

}