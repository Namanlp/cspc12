//
// Created by naman on 13/04/22.
//

//Write a function, strdel, that accepts a string and a character. The function
//returns string with the first occurence of character removed.

#include <stdio.h>

void strDel(int len, char str[], char character){
    int i =0;
    while(i < len && str[i] != character)
        i++;

    while(i < len){
        str[i] = str[i + 1];
        i++;
    }
}

int main(){
    int len = 0;
    printf("Enter length of string : ");
    scanf(" %d", &len);
    char str[len], character;
    printf("Enter String : ");
    scanf(" %s", str);
    printf("Enter Character : ");
    scanf(" %c", &character);

    strDel(len, str, character);
    printf( "New string is  : %s ", str);
}