//
// Created by naman on 13/04/22.
//
// VI. Write a function, strndel, that accepts a string and two integers, start and length.
//Return a new string that is equivalent to the original string, except that length
//characters beginning at start have been removed.

#include<stdio.h>

void strNDel(int len, int start, int length, char str[]){
    int i =0;
    while(i < start )
        i++;

    while(i < len-length ){
        str[i] = str[i + length];
        i++;
    }
    str[i] = '\0';
}

int main(){
    int len = 0;
    printf("Enter length of string : ");
    scanf(" %d", &len);
    char str[len];
    int start = 0, length = 0;
    printf("Enter String : ");
    scanf(" %s", str);
    printf("Enter start : ");
    scanf(" %d", &start);
    printf("Enter length : ");
    scanf(" %d", &length);

    strNDel(len, start, length, str );
    printf( "New string is  : %s ", str);
}
